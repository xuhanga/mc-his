﻿<%@ page language="C#" autoeventwireup="true" inherits="Booking_Touch_Dashboard_List, App_Web_0ed0zj05" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title></title>
</head>

<body class="body">

    <header>

        <div class="topbar">
            <a href="Default.aspx" class="back_btn"><i class="icon iconfont">&#xf0115;</i></a>
            <a href="javascript:;" class="top_home"></a>
            <h1 class="page_title"><asp:Label ID="lblTitle" runat="server"></asp:Label></h1>
        </div>

    </header>

    <form id="form1" runat="server">
    
        <div class="news_search">
            <div class="news_search_div">
                <asp:TextBox ID="txtKeyword" runat="server" CssClass="news_search_txt" placeholder="搜索" autocomplete="off"></asp:TextBox>
                <asp:LinkButton ID="lnbSearch" runat="server" CssClass="news_search_btn" onclick="lnbSearch_Click"><i class="icon iconfont">&#xf00a8;</i></asp:LinkButton>
            </div>
        </div>
    
        <div id="ListDiv" runat="server" class="list">

        </div>
        
        <div id="pager">
            <webdiyer:AspNetPager ID="ListPager" runat="server" OnPageChanged="ListPager_PageChanged"></webdiyer:AspNetPager>
        </div>
        
        <asp:TextBox ID="txtID" runat="server" style="display:none"></asp:TextBox>
        
        <div id="EditPanel" style="display:none;">
            <div id="shut" class="edit-shut" style="min-height:1200px;"></div>
            <div class="edit-wrap" style="top:150px; height:150px;">
                <div id="title" class="edit-title">删除预约</div>
                <div style="padding:20px; text-align:center">
                    确定删除该预约吗？
                </div>
                <div class="edit-button">
                    <asp:LinkButton ID="lnbDelete" runat="server" class="add_cart" style="width:80px; line-height:30px; height:30px; margin:0;" OnClick="lnbDelete_Click">确定</asp:LinkButton>&nbsp;
                    <a href="javascript:void(0);" onclick="document.getElementById('EditPanel').style.display='none'" class="liji_buy" style="width:80px; line-height:30px; height:30px; margin:0; background:#999;">取消</a>
                </div>
            </div>
        </div>
         
    </form>

    <script>

        function delBooking(id) {
            document.getElementById('txtID').value = id;
            document.getElementById('EditPanel').style.display = '';
        }

    </script>
 
</body>

</html>