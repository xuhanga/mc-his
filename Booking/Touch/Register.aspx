﻿<%@ page language="C#" autoeventwireup="true" inherits="Booking_Touch_Register, App_Web_ojkl0tgs" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <link rel="apple-touch-icon" href="Images/icon_192px.png" />
    <link rel="apple-touch-icon-precomposed" href="Images/icon_192px.png" />
    <meta name="format-detection" content="telephone=no" />
    <title>注册</title>
    <link rel="shortcut icon" href="../../Images/favicon.ico" type="image/x-icon" />
    
    <style type="text/css">
        body{-webkit-touch-callout:none; -webkit-user-select:auto; -moz-user-select:none; font-family:"Microsoft YaHei"; background:#F8F8F8;}
        .index_wrapper{background:#F8F8F8;}
        .table{width:100%; margin:0 auto;}
        .table td{ padding:5px;}
        .name{width:80px; text-align:left; font-size:12pt; color:#999;}
        .input_style{ width:100%; border:0px; border-bottom:solid 1px #ccc; padding:10px 0; background:url(); font-size:12pt; font-weight:700; outline:none;}
        .btn{ width:100%; padding:10px; font-size:12pt; border:0px;}
        a{font-size:10pt !important; color:#7A96BB;}
        #AlertDiv{ padding:20px; color:#ff0000}
    </style>
      
</head>

<body>

    <div class="index_wrapper">

        <form id="form1" runat="server">

            <div style="text-align:center; padding:0 20px; padding-top:20px;">
        
                <table class="table">
                    <tr>
                        <td class="name">真实姓名</td>
                        <td colspan="2">
                            <asp:TextBox ID="txtFullName" runat="server" CssClass="input_style" autocomplete="off"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="name">手机号码</td>
                        <td colspan="2">
                            <asp:TextBox ID="txtPhone1" runat="server" CssClass="input_style" autocomplete="off"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="name">确认手机</td>
                        <td colspan="2">
                            <asp:TextBox ID="txtPhone2" runat="server" CssClass="input_style" autocomplete="off"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="name">登录密码</td>
                        <td colspan="2">
                            <asp:TextBox ID="txtPassword1" runat="server" CssClass="input_style" TextMode="Password"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="name">确认密码</td>
                        <td colspan="2">
                            <asp:TextBox ID="txtPassword2" runat="server" CssClass="input_style" TextMode="Password"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="name">性别</td>
                        <td colspan="2">
                            <asp:RadioButtonList ID="rblSex" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Selected="True" Text="男"></asp:ListItem>
                                <asp:ListItem Text="女"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td class="name">年龄</td>
                        <td colspan="2">
                            <asp:TextBox ID="txtAge" runat="server" CssClass="input_style" autocomplete="off"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="name">住址</td>
                        <td colspan="2">
                            <asp:TextBox ID="txtAddress" runat="server" CssClass="input_style" autocomplete="off"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            
                <div style="text-align:center; padding-top:20px;">
                    <asp:Button ID="btnLogin" runat="server" Text="注 册" CssClass="btn bgcolor1" onclick="btnLogin_Click" />
                </div>
                
                <div style="text-align:center; padding-top:20px;">
                    <a href="Login.aspx" style="text-decoration:none">登录账号</a>
                </div>
                
                <div id="AlertDiv" runat="server"></div>

            </div>

        </form>
        
    </div>

</body>

</html>