﻿<%@ control language="C#" autoeventwireup="true" inherits="Document_Nav, App_Web_uyjveoqd" %>

<div class="col-md-3">
    <asp:HyperLink ID="hlAdd" runat="server" CssClass="btn btn-primary btn-block margin-bottom"><i class="fa fa-upload"></i> 上传</asp:HyperLink>
    <div class="box box-solid">
    <div class="box-header with-border">
        <h3 class="box-title">云盘</h3>
        <div class="box-tools">
        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div id="NavDiv" runat="server" class="box-body no-padding">

    </div>
    </div>
    <div class="box box-solid">
    <div class="box-header with-border">
        <h3 class="box-title">类型</h3>
        <div class="box-tools">
        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div id="TypeDiv" runat="server" class="box-body no-padding">

    </div>
    </div>
</div>
