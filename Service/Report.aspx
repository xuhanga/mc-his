﻿<%@ page language="C#" masterpagefile="../Commons/Main.master" autoeventwireup="true" inherits="Service_Report, App_Web_fmle4abu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" Runat="Server">

      <div class="content-wrapper">
      
        <section class="content-header">
          <h1>
            客服报表
            <asp:Label ID="lblNav" runat="server"></asp:Label>
          </h1>
          <ol class="breadcrumb">
            <li><a href="../"><i class="fa fa-home"></i> 首页</a></li>
            <li class="active">客服报表</li>
          </ol>
        </section>

        <section class="content">

        <div class="row">

            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-headphones"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">客服总数</span>
                  <a href="List.aspx?active=159,160"><asp:Label ID="Label1" runat="server" CssClass="info-box-number"></asp:Label></a>
                </div>
              </div>
            </div>
            
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="fa fa-plus"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">新客服</span>
                  <a href="List.aspx?active=159,160"><asp:Label ID="Label2" runat="server" CssClass="info-box-number"></asp:Label></a>
                </div>
              </div>
            </div>

            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-clock-o"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">进行中</span>
                  <a href="List.aspx?active=159,160"><asp:Label ID="Label3" runat="server" CssClass="info-box-number"></asp:Label></a>
                </div>
              </div>
            </div>

            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-green"><i class="fa fa-check"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">已完成</span>
                  <a href="List.aspx?active=159,160"><asp:Label ID="Label4" runat="server" CssClass="info-box-number"></asp:Label></a>
                </div>
              </div>
            </div>

        </div>
            
        <div class="row">
            
        <section class="col-lg-6 connectedSortable">

          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">客服统计</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
              
            </div>
            <div class="tab-content">

				<div id="chart3" style="height:350px; margin:10px"></div>

            </div>
          </div>

        </section>

        <section class="col-lg-6 connectedSortable">

          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">占比统计</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
              
            </div>
            <div class="tab-content">

				<div id="chart4" style="height:350px; margin:10px"></div>

            </div>
          </div>

        </section>

        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                <div class="box-header">
                    <h3 class="box-title">
                        客服年度报告
                    </h3>
                </div>
                <div class="box-body table-responsive no-padding">
                
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" GridLines="None" BorderWidth="0px" CssClass="table table-hover report-tb">
                    </asp:GridView>

                </div>
                
                </div>
            </div>
        </div>

    </section>

    </div>
    
    <div id="ChartDiv" runat="server"></div>

</asp:Content>