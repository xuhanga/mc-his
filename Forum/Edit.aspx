﻿<%@ page language="C#" masterpagefile="~/Commons/Main.master" autoeventwireup="true" inherits="Forum_Edit, App_Web_xkhhzvu0" %>

<%@ Register Src="~/Controls/CKeditor.ascx" TagName="CKeditor" TagPrefix="MojoCube" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" Runat="Server">

    <script type="text/javascript">

        function ddlChange() {
            var ddl = document.getElementById("ctl00_cphMain_ddlType");
            if (ddl.value == "2") {
                document.getElementById("ctl00_cphMain_VoteDiv").style.display = "";
            }
            else {
                document.getElementById("ctl00_cphMain_VoteDiv").style.display = "none";
            }
        }

        function removeAtt() {
            document.getElementById("ctl00_cphMain_txtFilePath").value = "";
        }

    </script>

      <div class="content-wrapper">
      
        <section class="content-header">
          <h1>
            主题编辑
          </h1>
          <ol class="breadcrumb">
            <li><a href="../"><i class="fa fa-home"></i> 首页</a></li>
            <li class="active">主题编辑</li>
          </ol>
        </section>

        <section class="content">
        
            <div id="AlertDiv" runat="server"></div>

            <div class="box box-default">
                
                <div class="box-header with-border">
                    <h3 class="box-title">
                        <asp:HyperLink ID="hlBack" runat="server"><span class="label label-back"><i class="fa fa-chevron-left"></i> 返回</span></asp:HyperLink>
                    </h3>
                </div>

                <div class="box-body">
                    <div class="form-group">
                        <asp:DropDownList ID="ddlType" runat="server" CssClass="form-control select2" onchange="ddlChange();"></asp:DropDownList>
                    </div>
                    <div id="VoteDiv" runat="server" class="form-group" style="position:relative; display:none">
                        
                        <asp:TextBox ID="txtVote" runat="server" CssClass="form-control" placeholder="投票标题："></asp:TextBox>
                        <div style="position:absolute; top:5px; right:5px;">
                            <asp:LinkButton ID="lnbAddVote" runat="server" onclick="lnbAddVote_Click"><span class="label label-success"><i class="fa fa-plus"></i> 新增</span></asp:LinkButton>
                        </div>
                        
                        <asp:Label ID="lblVoteInfo" runat="server"></asp:Label>

                        <div style="background:#eee; overflow:auto">
                            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" GridLines="None" BorderWidth="0px" CssClass="table" OnRowDataBound="GridView1_RowDataBound" OnRowCommand="GridView1_RowCommand" OnRowCreated="GridView1_RowCreated">
                                <Columns>
                                    <asp:TemplateField HeaderText="投票标题">
                                        <ItemTemplate>
                                            <asp:Label ID="lblID" runat="server" Text='<%# Bind("pk_Vote") %>' Visible="false"></asp:Label>
                                            <asp:TextBox ID="txtVoteGV" runat="server" Text='<%# Bind("Title") %>' CssClass="form-control"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="显示">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtVisual" runat="server" Text='<%# Bind("Visual") %>' CssClass="form-control"></asp:TextBox>
                                        </ItemTemplate>
                                        <ItemStyle Width="150px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="操作">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="gvSave" runat="server" CommandName="_save"><span class="label label-primary"><i class="fa fa-save"></i> 保存</span></asp:LinkButton>
                                            <asp:LinkButton ID="gvDelete" runat="server" ToolTip="删除" CommandName="_delete"><span class="label label-danger"><i class="fa fa-remove"></i> 删除</span></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle Width="150px" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>

                    </div>
                    <div class="form-group">
                        <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control select2"></asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <asp:TextBox ID="txtTitle" runat="server" CssClass="form-control" placeholder="标题："></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <MojoCube:CKeditor id="txtContent" runat="server" Height="400" />
                    </div>
                    <div class="form-group" style="position:relative;">
                        <asp:TextBox ID="txtReceiver" runat="server" CssClass="form-control" placeholder="个别可见：" onfocus="this.blur()"></asp:TextBox>
                        <asp:TextBox ID="txtReceiverID" runat="server" style="display:none;"></asp:TextBox>
                        <div style="position:absolute; top:5px; right:5px;">
                            <asp:HyperLink ID="hlAdd" runat="server" CssClass="fancybox fancybox.iframe"><span class="label label-success"><i class="fa fa-plus"></i> 权限</span></asp:HyperLink>
                        </div>
                    </div>
                    <div class="form-group" style="position:relative;">
                        <asp:TextBox ID="txtFilePath" runat="server" CssClass="form-control" onfocus="this.blur()"></asp:TextBox>
                        <div style="position:absolute; top:5px; right:5px;">
                            <a href="javascript:void();" onclick="removeAtt();"><span class="label label-danger"><i class="fa fa-remove"></i> 移除</span></a>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="btn btn-default btn-file">
                            <i class="fa fa-paperclip"></i> 增加附件
                            <asp:FileUpload ID="fuAttachment" runat="server" onchange="ChkUpload(this);"></asp:FileUpload>
                            <span id="filepath"></span>
                        </div>
                        <p class="help-block">5MB以内</p>
                    </div>
                </div>

                <div class="box-footer">
                    <asp:Button ID="btnSave" runat="server" Text="保存" CssClass="btn btn-primary" onclick="btnSave_Click"></asp:Button>
                    <asp:Button ID="btnCancel" runat="server" Text="取消" CssClass="btn btn-default" onclick="btnCancel_Click"></asp:Button>
                </div>

            </div>

        </section>

      </div>

</asp:Content>


