﻿<%@ page language="C#" masterpagefile="~/Commons/Main.master" autoeventwireup="true" inherits="Supplier_Edit, App_Web_tyjte3vi" %>

<%@ Register Src="~/Controls/KindEditor.ascx" TagName="KindEditor" TagPrefix="MojoCube" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" Runat="Server">

      <div class="content-wrapper">
      
        <section class="content-header">
          <h1>
            供应商管理
          </h1>
          <ol class="breadcrumb">
            <li><a href="../"><i class="fa fa-home"></i> 首页</a></li>
            <li class="active">供应商管理</li>
          </ol>
        </section>

        <section class="content">

          <div id="AlertDiv" runat="server"></div>

          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">
                  <asp:HyperLink ID="hlBack" runat="server"><span class="label label-back"><i class="fa fa-chevron-left"></i> 返回</span></asp:HyperLink>
              </h3>
            </div>

            <div class="box-body">

              <div class="row">
              
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label29" runat="server" Text="类型"></asp:Label></label>
                    <asp:DropDownList ID="ddlType" runat="server" CssClass="form-control select2"></asp:DropDownList>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label30" runat="server" Text="状态"></asp:Label></label>
                    <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control select2"></asp:DropDownList>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label1" runat="server" Text="供应商名称"></asp:Label></label>
                    <asp:TextBox ID="txtFullName" runat="server" CssClass="form-control" placeholder="如：魔方动力软件有限公司"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label3" runat="server" Text="供应商简拼"></asp:Label></label>
                    <asp:TextBox ID="txtShortName" runat="server" CssClass="form-control" placeholder="如：MFDL"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label5" runat="server" Text="供应商类型"></asp:Label></label>
                    <asp:TextBox ID="txtSupplierType" runat="server" CssClass="form-control" placeholder="如：合作伙伴、供应商、竞争者..."></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label7" runat="server" Text="供应商标签"></asp:Label></label>
                    <asp:TextBox ID="txtTags" runat="server" CssClass="form-control" placeholder="如：软件、科技、外包..."></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label2" runat="server" Text="供应商级别"></asp:Label></label>
                    <asp:DropDownList ID="ddlSupplierLevel" runat="server" CssClass="form-control select2">
                        <asp:ListItem Text="1星" Value="1星" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="2星" Value="2星"></asp:ListItem>
                        <asp:ListItem Text="3星" Value="3星"></asp:ListItem>
                        <asp:ListItem Text="4星" Value="4星"></asp:ListItem>
                        <asp:ListItem Text="5星" Value="5星"></asp:ListItem>
                    </asp:DropDownList>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label4" runat="server" Text="供应商属性"></asp:Label></label>
                    <asp:TextBox ID="txtSupplierProperty" runat="server" CssClass="form-control" placeholder="如：意向供应商、合作供应商..."></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label8" runat="server" Text="供应商状态"></asp:Label></label>
                    <asp:TextBox ID="txtSupplierState" runat="server" CssClass="form-control" placeholder="如：潜在、有意向、失败、已成交..."></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label9" runat="server" Text="供应商来源"></asp:Label></label>
                    <asp:TextBox ID="txtSupplierSource" runat="server" CssClass="form-control" placeholder="如：电话来访、供应商介绍..."></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label10" runat="server" Text="所属行业"></asp:Label></label>
                    <asp:TextBox ID="txtIndustry" runat="server" CssClass="form-control" placeholder="如：科技软件、食品加工..."></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label11" runat="server" Text="公司性质"></asp:Label></label>
                    <asp:TextBox ID="txtNature" runat="server" CssClass="form-control" placeholder="如：国有企业、民营企业..."></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label12" runat="server" Text="员工数量"></asp:Label></label>
                    <asp:TextBox ID="txtEmployeNum" runat="server" CssClass="form-control" placeholder="如：50人以下、50-100人..."></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label13" runat="server" Text="营业执照"></asp:Label></label>
                    <asp:TextBox ID="txtLicense" runat="server" CssClass="form-control" placeholder="如：110108001885980"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label14" runat="server" Text="公司网址"></asp:Label></label>
                    <asp:TextBox ID="txtWebsite" runat="server" CssClass="form-control" placeholder="如：http://www.mojocube.com/"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label6" runat="server" Text="供应商备注"></asp:Label></label>
                    <asp:TextBox ID="txtRemark" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
              </div>
                
              <hr style="border:solid 1px #6C7B8B" />
                  
              <div class="row">
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label15" runat="server" Text="联系人"></asp:Label></label>
                    <asp:TextBox ID="txtContact" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label16" runat="server" Text="手机号码"></asp:Label></label>
                    <asp:TextBox ID="txtMobile" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label17" runat="server" Text="办公电话"></asp:Label></label>
                    <asp:TextBox ID="txtTEL" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label18" runat="server" Text="传真"></asp:Label></label>
                    <asp:TextBox ID="txtFax" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label19" runat="server" Text="Email"></asp:Label></label>
                    <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label20" runat="server" Text="QQ"></asp:Label></label>
                    <asp:TextBox ID="txtQQ" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label21" runat="server" Text="微信"></asp:Label></label>
                    <asp:TextBox ID="txtWechat" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label22" runat="server" Text="旺旺"></asp:Label></label>
                    <asp:TextBox ID="txtWangwang" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
              </div>
                
              <hr style="border:solid 1px #6C7B8B" />
                  
              <div class="row">
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label23" runat="server" Text="国家"></asp:Label></label>
                    <asp:TextBox ID="txtCountry" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label24" runat="server" Text="省"></asp:Label></label>
                    <asp:TextBox ID="txtProvince" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label25" runat="server" Text="市"></asp:Label></label>
                    <asp:TextBox ID="txtCity" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label26" runat="server" Text="区"></asp:Label></label>
                    <asp:TextBox ID="txtCounty" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label27" runat="server" Text="地址"></asp:Label></label>
                    <asp:TextBox ID="txtAddress" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label28" runat="server" Text="邮编"></asp:Label></label>
                    <asp:TextBox ID="txtZipCode" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
              </div>
                
              <hr style="border:solid 1px #6C7B8B; display:none" />
                  
              <div class="row hidden">
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label31" runat="server" Text="到期日期"></asp:Label></label>
                    <asp:TextBox ID="txtEndDate" runat="server" CssClass="form-control" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'})" AutoComplete="off"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label32" runat="server" Text="提醒日期"></asp:Label></label>
                    <asp:TextBox ID="txtAlertDate" runat="server" CssClass="form-control" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'})" AutoComplete="off"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label33" runat="server" Text="提醒内容"></asp:Label></label>
                    <asp:TextBox ID="txtAlertInfo" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label34" runat="server" Text="是否提醒"></asp:Label></label>
                    <br />
                    <asp:CheckBox ID="cbAlert" runat="server"></asp:CheckBox>
                  </div>
                  
              </div>

              <div class="row">
                    
                  <div class="form-group" style="padding:15px">
                      <MojoCube:KindEditor id="txtContent" runat="server" Height="500" />
                  </div>
                        
              </div>
                    
            </div>
            
            <div class="box-footer">
                <asp:Button ID="btnSave" runat="server" Text="保存" CssClass="btn btn-primary" onclick="btnSave_Click"></asp:Button>
                <asp:Button ID="btnCancel" runat="server" Text="取消" CssClass="btn btn-default" onclick="btnCancel_Click"></asp:Button>
            </div>

          </div>

        </section>

      </div>

</asp:Content>