﻿<%@ page language="C#" autoeventwireup="true" inherits="Chat_Operation, App_Web_4p0w1hca" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <%=MainCss%>
    
    <script type="text/javascript">

        function setColor(color) {

            document.getElementById('txtColor').value = color;
            __doPostBack("lnbSave", "");

        }

    </script>

</head>
<body style="background:#F5F5F5">
    <form id="form1" runat="server">
        
        <div class="top">
            <div style="padding:0px 15px; line-height:50px; font-size:13pt;">
                个性设置
            </div>
        </div>

        <div style="padding-top:200px;">
    
            <div id="ColorDiv" runat="server" style="clear:both; overflow:auto; width:364px; margin:0 auto;"></div>

            <div style="text-align:center; color:#999; padding-top:10px;">
                请选择您喜欢的主题颜色
            </div>

            <div style="display:none">

                <asp:TextBox ID="txtColor" runat="server"></asp:TextBox>
                <asp:LinkButton ID="lnbSave" runat="server" OnClick="lnbSave_Click"></asp:LinkButton>

            </div>
            
        </div>

    </form>
</body>
</html>