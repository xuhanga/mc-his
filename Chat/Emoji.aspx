﻿<%@ page language="C#" autoeventwireup="true" inherits="Chat_Emoji, App_Web_4p0w1hca" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <%=MainCss%>

    <script>

        function setEmoji(id)
        {
            window.parent.document.getElementById('txtFile').value = "<img src='../../Chat/Emoji/" + id + ".gif' />";
            window.parent.sendMsg();
            window.parent.$.fancybox.close();
        }

    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div style="min-height:300px;">

        <div class="emoji">
            <a href="javascript:;" onclick="setEmoji(1)"><img src="Emoji/1.gif" /></a>
        </div>
    
        <div class="emoji">
            <a href="javascript:;" onclick="setEmoji(2)"><img src="Emoji/2.gif" /></a>
        </div>
    
        <div class="emoji">
            <a href="javascript:;" onclick="setEmoji(3)"><img src="Emoji/3.gif" /></a>
        </div>
    
        <div class="emoji">
            <a href="javascript:;" onclick="setEmoji(4)"><img src="Emoji/4.gif" /></a>
        </div>
    
        <div class="emoji">
            <a href="javascript:;" onclick="setEmoji(5)"><img src="Emoji/5.gif" /></a>
        </div>
    
        <div class="emoji">
            <a href="javascript:;" onclick="setEmoji(6)"><img src="Emoji/6.gif" /></a>
        </div>
    
        <div class="emoji">
            <a href="javascript:;" onclick="setEmoji(7)"><img src="Emoji/7.gif" /></a>
        </div>
    
        <div class="emoji">
            <a href="javascript:;" onclick="setEmoji(8)"><img src="Emoji/8.gif" /></a>
        </div>
    
    </div>
    </form>
</body>
</html>
