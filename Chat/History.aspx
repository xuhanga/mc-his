﻿<%@ page language="C#" autoeventwireup="true" inherits="Chat_History, App_Web_4p0w1hca" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <%=MainCss%>
    <link href="Icon/Ali1/iconfont.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        
        <div class="news_search" style="width:95%">
            <div class="news_search_div">
                <asp:TextBox ID="txtKeyword" runat="server" CssClass="news_search_txt" placeholder="搜索" autocomplete="off" Width="90%"></asp:TextBox>
                <asp:LinkButton ID="lnbSearch" runat="server" CssClass="news_search_btn" onclick="lnbSearch_Click"><i class="icon iconfont" style="color:#333">&#xf00a8;</i></asp:LinkButton>
            </div>
        </div>
    
        <div id="ChatHistory" runat="server"></div>
    
        <div id="pager">
            <webdiyer:AspNetPager ID="ListPager" runat="server" OnPageChanged="ListPager_PageChanged"></webdiyer:AspNetPager>
        </div>
        
    </div>
    </form>
</body>
</html>
