﻿<%@ page language="C#" autoeventwireup="true" inherits="Chat_Upload, App_Web_4p0w1hca" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <%=MainCss%>
    <link href="Icon/Ali1/iconfont.css" rel="stylesheet" />
    <script src="JS/jquery.min.js"></script>
    <script src="JS/mojocube.js?v=1"></script>
    
    <script type="text/javascript">

        function uploadFile(filePath) {
            if (filePath.length > 0) {
                ShowLoading(document.getElementById('btnUploadFile'), '上传中，请稍等...');
                __doPostBack('btnUploadFile', '');
            }
        }

        function sendMsg(url) {
            window.parent.document.getElementById('txtFile').value = url;
            window.parent.sendMsg();
            window.parent.$.fancybox.close();
        }

    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div style="height:300px; border:dashed 3px #ddd;">

        <asp:Panel ID="Panel1" runat="server" style="padding-top:80px; clear:both; text-align:center">
            
            <div class="div3" style="padding-top:50px; width:150px; height:150px; margin:0 auto;">
                <a class="fontcolor1 icon_btn"><i class="icon iconfont big" style="font-size:120pt; color:#ccc;">&#xf00df;</i></a>
                <asp:FileUpload ID="fuAttachment" runat="server" CssClass="inputstyle2" style="width:150px; height:150px;" onchange="uploadFile(this.value)" />
            </div>
                
        </asp:Panel>

        <asp:Panel ID="Panel2" runat="server" Visible="false">

            <div style="padding:10px; text-align:center">
                <div style="padding:10px;">
                    <asp:Image ID="Image1" runat="server" style="height:150px;" />
                </div>
                <div style="padding:10px">
                    <asp:Label ID="lblTitle" runat="server"></asp:Label>
                </div>
                <div style="padding:10px;">
                    <asp:Button ID="btnSend" runat="server" Text="发送" OnClick="btnSend_Click" style="width:100px; padding:5px; background:#E9E9E9; border:solid 1px #ddd" />
                </div>
            </div>

        </asp:Panel>
    
        <input type="button" name="btnUploadFile" id="btnUploadFile" runat="server" value="" class="button1" style="display:none;" />

    </div>
    </form>
</body>
</html>
