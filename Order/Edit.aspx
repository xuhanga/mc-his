﻿<%@ page language="C#" masterpagefile="~/Commons/Main.master" autoeventwireup="true" inherits="Order_Edit, App_Web_nrpmib2t" %>

<%@ Register Src="~/Controls/KindEditor.ascx" TagName="KindEditor" TagPrefix="MojoCube" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" Runat="Server">

      <div class="content-wrapper">
      
        <section class="content-header">
          <h1>
            诊断管理
          </h1>
          <ol class="breadcrumb">
            <li><a href="../"><i class="fa fa-home"></i> 首页</a></li>
            <li class="active">诊断管理</li>
          </ol>
        </section>

        <section class="content">

          <div id="AlertDiv" runat="server"></div>

          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">
                  <asp:HyperLink ID="hlBack" runat="server"><span class="label label-back"><i class="fa fa-chevron-left"></i> 返回</span></asp:HyperLink>
                  <asp:HyperLink ID="hlAddProduct" runat="server" Visible="false"><span class="label label-success"><i class="fa fa-plus"></i> 药品</span></asp:HyperLink>
                  <asp:HyperLink ID="hlAddContract" runat="server" Visible="false"><span class="label label-success"><i class="fa fa-plus"></i> 合同</span></asp:HyperLink>
                  <asp:HyperLink ID="hlAddInvoice" runat="server" Visible="false"><span class="label label-success"><i class="fa fa-plus"></i> 发票</span></asp:HyperLink>
              </h3>
            </div>

            <div class="box-body">

              <div class="row">
              
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label29" runat="server" Text="类型"></asp:Label></label>
                    <asp:DropDownList ID="ddlType" runat="server" CssClass="form-control select2"></asp:DropDownList>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label30" runat="server" Text="状态"></asp:Label></label>
                    <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control select2"></asp:DropDownList>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label1" runat="server" Text="编号"></asp:Label></label>
                    <asp:TextBox ID="txtOrderNumber" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label3" runat="server" Text="标题"></asp:Label></label>
                    <asp:TextBox ID="txtTitle" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label2" runat="server" Text="诊断"></asp:Label></label>
                    <asp:TextBox ID="txtRemark" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="10"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group" style="position:relative;">
                    <label><asp:Label ID="Label4" runat="server" Text="患者"></asp:Label></label>
                    <asp:HyperLink ID="hlCustomer" runat="server" Target="_blank" Visible="false"><i class="fa fa-external-link"></i></asp:HyperLink>
                    <asp:TextBox ID="txtReceiver" runat="server" CssClass="form-control" onfocus="this.blur()"></asp:TextBox>
                    <asp:TextBox ID="txtReceiverID" runat="server" style="display:none;"></asp:TextBox>
                    <div style="position:absolute; top:31px; right:20px;">
                        <asp:HyperLink ID="hlSearch" runat="server" CssClass="fancybox fancybox.iframe"><span class="label label-success"><i class="fa fa-search"></i> 查找</span></asp:HyperLink>
                    </div>
                  </div>
                  
              </div>
                
              <hr style="border:solid 1px #6C7B8B; display:none" />
                  
              <div class="row hidden">
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label15" runat="server" Text="联系人"></asp:Label></label>
                    <asp:TextBox ID="txtContact" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label7" runat="server" Text="性别"></asp:Label></label>
                    <asp:DropDownList ID="ddlSex" runat="server" CssClass="form-control select2">
                        <asp:ListItem Text="男" Value="0" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="女" Value="1"></asp:ListItem>
                    </asp:DropDownList>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label16" runat="server" Text="手机号码"></asp:Label></label>
                    <asp:TextBox ID="txtMobile" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label17" runat="server" Text="办公电话"></asp:Label></label>
                    <asp:TextBox ID="txtTEL" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label19" runat="server" Text="Email"></asp:Label></label>
                    <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label20" runat="server" Text="QQ"></asp:Label></label>
                    <asp:TextBox ID="txtQQ" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label21" runat="server" Text="微信"></asp:Label></label>
                    <asp:TextBox ID="txtWechat" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label22" runat="server" Text="旺旺"></asp:Label></label>
                    <asp:TextBox ID="txtWangwang" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
              </div>
                
              <hr style="border:solid 1px #6C7B8B; display:none" />
                  
              <div class="row hidden">
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label23" runat="server" Text="国家"></asp:Label></label>
                    <asp:TextBox ID="txtCountry" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label24" runat="server" Text="省"></asp:Label></label>
                    <asp:TextBox ID="txtProvince" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label25" runat="server" Text="市"></asp:Label></label>
                    <asp:TextBox ID="txtCity" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label26" runat="server" Text="区"></asp:Label></label>
                    <asp:TextBox ID="txtCounty" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label27" runat="server" Text="地址"></asp:Label></label>
                    <asp:TextBox ID="txtAddress" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label28" runat="server" Text="邮编"></asp:Label></label>
                    <asp:TextBox ID="txtZipCode" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
              </div>
                
              <hr style="border:solid 1px #6C7B8B" />
                  
              <div class="row">
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label5" runat="server" Text="会诊科室"></asp:Label></label>
                    <asp:DropDownList ID="ddlExpress" runat="server" CssClass="form-control select2" Visible="false"></asp:DropDownList>
                    <asp:TextBox ID="txtLogisticInfo" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label33" runat="server" Text="住院床号"></asp:Label></label>
                    <asp:HyperLink ID="hlExpress" runat="server" Visible="false"><i class="fa fa-external-link"></i></asp:HyperLink>
                    <asp:TextBox ID="txtLogisticCode" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label6" runat="server" Text="挂号费"></asp:Label></label>
                    <asp:TextBox ID="txtFreight" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label8" runat="server" Text="附加费"></asp:Label></label>
                    <asp:TextBox ID="txtPremium" runat="server" CssClass="form-control"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label31" runat="server" Text="付款日期"></asp:Label></label>
                    <asp:TextBox ID="txtPayDate" runat="server" CssClass="form-control" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'})" AutoComplete="off"></asp:TextBox>
                  </div>
                  
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label34" runat="server" Text="是否付款"></asp:Label></label>
                    <br />
                    <asp:CheckBox ID="cbPay" runat="server"></asp:CheckBox>
                  </div>
                  
              </div>

              <div class="row">
                    
                  <div class="form-group" style="padding:15px">
                      <MojoCube:KindEditor id="txtContent" runat="server" Height="500" />
                  </div>
                        
              </div>
                    
            </div>
            
            <div class="box-footer">
                <asp:Button ID="btnSave" runat="server" Text="保存" CssClass="btn btn-primary" onclick="btnSave_Click"></asp:Button>
                <asp:Button ID="btnCancel" runat="server" Text="取消" CssClass="btn btn-default" onclick="btnCancel_Click"></asp:Button>
            </div>

          </div>

        </section>

      </div>

</asp:Content>