
document.write("<div class='loading-bg'><div class='loading'></div></div>");

function showLoader() {
    $(".loading-bg").show();
}

//一般打印
function McPrint() {
    window.print();
}

//隐藏特定元素打印，如.btn，多个元素用逗号隔开，如.btn1,.btn2
function McPrintHide(element) {
    $(element).hide();
    $('.box').css("border-top", "3px solid #ffffff");
    window.print();
    $('.box').css("border-top", "3px solid #d2d6de");
    $(element).show();
}

//上传图片检查并显示预览图
function ChkUploadImage(ct_file, ct_image) {
    var fileext = ct_file.value.substring(ct_file.value.lastIndexOf("."), ct_file.value.length);
    fileext = fileext.toLowerCase();
    if ((fileext != '.jpg') && (fileext != '.gif') && (fileext != '.jpeg') && (fileext != '.png')) {
        alert("Please upload image.");
        ct_file.focus();
        ct_file.outerHTML = ct_file.outerHTML;
    }
    else {
        ct_image.style.display = "block";
        ct_image.src = window.webkitURL.createObjectURL(ct_file.files[0])
    }
}

//检查上传文件类型
function ChkUpload(ct_file) {
    var fileext = ct_file.value.substring(ct_file.value.lastIndexOf("."), ct_file.value.length);
    fileext = fileext.toLowerCase();
    if ((fileext == '.exe') || (fileext == '.asp') || (fileext == '.aspx') || (fileext == '.php')) {
        alert("Illegal file type!");
        ct_file.focus();
        ct_file.outerHTML = ct_file.outerHTML;
    }
    else {
        if (document.getElementById('filepath') != null) {
            document.getElementById('filepath').innerText = ct_file.value.substring(ct_file.value.lastIndexOf("\\") + 1, ct_file.value.length);
        }
    }
}

//新增接收人
function setReceiver1(name, ids) {

    var txtName = window.parent.document.getElementById('ctl00_cphMain_txtReceiver').value;
    var txtIDs = window.parent.document.getElementById('ctl00_cphMain_txtReceiverID').value;

    window.parent.document.getElementById('ctl00_cphMain_txtReceiver').value = name;
    window.parent.document.getElementById('ctl00_cphMain_txtReceiverID').value = ids;

    window.parent.$.fancybox.close();
}

//追加接收人
function setReceiver2(name, ids) {

    var txtName = window.parent.document.getElementById('ctl00_cphMain_txtReceiver').value;
    var txtIDs = window.parent.document.getElementById('ctl00_cphMain_txtReceiverID').value;

    if (txtName == "" && txtIDs == "") {
        window.parent.document.getElementById('ctl00_cphMain_txtReceiver').value = name;
        window.parent.document.getElementById('ctl00_cphMain_txtReceiverID').value = ids;
    }
    else {
        window.parent.document.getElementById('ctl00_cphMain_txtReceiver').value = txtName + '; ' + name;
        window.parent.document.getElementById('ctl00_cphMain_txtReceiverID').value = txtIDs + '|' + ids;
    }

    window.parent.$.fancybox.close();
}

//新增通知人
function setNotify1(name, ids) {

    var txtName = window.parent.document.getElementById('ctl00_cphMain_txtNotify').value;
    var txtIDs = window.parent.document.getElementById('ctl00_cphMain_txtNotifyID').value;

    window.parent.document.getElementById('ctl00_cphMain_txtNotify').value = name;
    window.parent.document.getElementById('ctl00_cphMain_txtNotifyID').value = ids;

    window.parent.$.fancybox.close();
}

//追加通知人
function setNotify2(name, ids) {

    var txtName = window.parent.document.getElementById('ctl00_cphMain_txtNotify').value;
    var txtIDs = window.parent.document.getElementById('ctl00_cphMain_txtNotifyID').value;

    if (txtName == "" && txtIDs == "") {
        window.parent.document.getElementById('ctl00_cphMain_txtNotify').value = name;
        window.parent.document.getElementById('ctl00_cphMain_txtNotifyID').value = ids;
    }
    else {
        window.parent.document.getElementById('ctl00_cphMain_txtNotify').value = txtName + '; ' + name;
        window.parent.document.getElementById('ctl00_cphMain_txtNotifyID').value = txtIDs + '|' + ids;
    }

    window.parent.$.fancybox.close();
}

//新增收信人
function setTo1(address) {

    var txtAddress = window.parent.document.getElementById('ctl00_cphMain_txtTo').value;

    window.parent.document.getElementById('ctl00_cphMain_txtTo').value = address;

    window.parent.$.fancybox.close();
}

//追加收信人
function setTo2(address) {

    var txtAddress = window.parent.document.getElementById('ctl00_cphMain_txtTo').value;

    if (txtAddress == "") {
        window.parent.document.getElementById('ctl00_cphMain_txtTo').value = address;
    }
    else {
        window.parent.document.getElementById('ctl00_cphMain_txtTo').value = txtAddress + '; ' + address;
    }

    window.parent.$.fancybox.close();
}

function openWin(url, iWidth, iHeight) {
    var iTop = (window.screen.availHeight - 30 - iHeight) / 2;
    var iLeft = (window.screen.availWidth - 10 - iWidth) / 2;
    var win = window.open(url, 'win_' + Math.random(), 'height=' + iHeight + ',,innerHeight=' + iHeight + ',width=' + iWidth + ',innerWidth=' + iWidth + ',top=' + iTop + ',left=' + iLeft + ',status=no,toolbar=no,menubar=no,location=no,resizable=no,scrollbars=0,titlebar=no');
    win.focus();
}