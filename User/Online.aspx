﻿<%@ page language="C#" masterpagefile="~/Commons/Main.master" autoeventwireup="true" inherits="User_Online, App_Web_rztyn4pz" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" Runat="Server">

      <div class="content-wrapper">

        <section class="content-header">
          <h1>
            在线用户
          </h1>
          <ol class="breadcrumb">
            <li><a href="../"><i class="fa fa-home"></i> 首页</a></li>
            <li class="active">在线用户</li>
          </ol>
        </section>

        <section class="content">

          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">
                      <asp:HyperLink ID="hlPrint" runat="server" NavigateUrl="javascript:McPrint();"><span class="label label-primary"><i class="fa fa-print"></i> 打印</span></asp:HyperLink>
                      <asp:HyperLink ID="hlSearch" runat="server" NavigateUrl="#SearchDiv" CssClass="fancybox" ToolTip="高级搜索"><span class="label label-back"><i class="fa fa-search"></i> 高级搜索</span></asp:HyperLink>
                  </h3>
                  <div class="box-tools">
                    <div class="input-group" style="width: 150px;">
                      <asp:TextBox ID="txtKeyword" runat="server" CssClass="form-control input-sm pull-right" placeholder="查找..."></asp:TextBox>
                      <div class="input-group-btn">
                        <asp:LinkButton ID="lnbSearch" runat="server" CssClass="btn btn-sm btn-default" onclick="lnbSearch_Click"><i class="fa fa-search"></i></asp:LinkButton>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="box-body table-responsive no-padding">

                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" GridLines="None" BorderWidth="0px" CssClass="table table-hover" AllowSorting="True" OnSorting="GridView1_Sorting" OnRowDataBound="GridView1_RowDataBound" OnRowCreated="GridView1_RowCreated">
                        <Columns>
                            <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <asp:Label ID="lblThumbnail" runat="server" Text='<%# Bind("ImagePath1") %>' style="margin:0px 10px"></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="50px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="姓名" SortExpression="FullName">
                                <ItemTemplate>
                                    <asp:Label ID="lblFullName" runat="server" Text='<%# Bind("FullName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="部门" SortExpression="DepartmentName">
                                <ItemTemplate>
                                    <asp:Label ID="lblDepartmentName" runat="server" Text='<%# Bind("DepartmentName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="用户名" SortExpression="UserName">
                                <ItemTemplate>
                                    <asp:Label ID="lblUserName" runat="server" Text='<%# Bind("UserName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="角色" SortExpression="RoleName_CHS">
                                <ItemTemplate>
                                    <asp:Label ID="lblRoleName" runat="server" Text='<%# Bind("RoleName_CHS") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="电话" SortExpression="Phone1">
                                <ItemTemplate>
                                    <asp:Label ID="lblPhone1" runat="server" Text='<%# Bind("Phone1") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="状态" SortExpression="Remark">
                                <ItemTemplate>
                                    <asp:Label ID="lblRemark" runat="server" Text='<%# Bind("Remark") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="最后更新" SortExpression="ModifyDate">
                                <ItemTemplate>
                                    <asp:Label ID="lblModifyDate" runat="server" Text='<%# Bind("ModifyDate") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    
                    <div id="pager">
                       <webdiyer:AspNetPager ID="ListPager" runat="server" OnPageChanged="ListPager_PageChanged"></webdiyer:AspNetPager>
                    </div>
        
                </div>
                
              </div>
            </div>
          </div>

        </section>

      </div>
    
      <div id="SearchDiv" style="display:none; width:500px;">
            
          <div class="box-body">
          
              <div class="col-md-6 form-group">
                <label><asp:Label ID="Label1" runat="server" Text="部门"></asp:Label></label>
                <asp:DropDownList ID="ddlSearchDepartment" runat="server" CssClass="form-control select2"></asp:DropDownList>
              </div>
                  
              <div class="col-md-6 form-group">
                <label><asp:Label ID="Label5" runat="server" Text="角色"></asp:Label></label>
                <asp:DropDownList ID="ddlSearchRole" runat="server" CssClass="form-control select2"></asp:DropDownList>
              </div>
                  
              <div class="col-md-6 form-group">
                <label><asp:Label ID="Label2" runat="server" Text="姓名"></asp:Label></label>
                <asp:TextBox ID="txtSearchFullName" runat="server" CssClass="form-control"></asp:TextBox>
              </div>
                  
              <div class="col-md-6 form-group">
                <label><asp:Label ID="Label3" runat="server" Text="用户名"></asp:Label></label>
                <asp:TextBox ID="txtSearchUserName" runat="server" CssClass="form-control"></asp:TextBox>
              </div>
                  
              <div class="col-md-6 form-group">
                <label><asp:Label ID="Label7" runat="server" Text="电话"></asp:Label></label>
                <asp:TextBox ID="txtSearchPhone" runat="server" CssClass="form-control"></asp:TextBox>
              </div>
                  
              <div class="col-md-6 form-group">
                <label><asp:Label ID="Label8" runat="server" Text="状态"></asp:Label></label>
                <asp:TextBox ID="txtSearchRemark" runat="server" CssClass="form-control"></asp:TextBox>
              </div>
                  
              <div class="col-md-6 form-group">
                <label><asp:Label ID="Label4" runat="server" Text="更新起"></asp:Label></label>
                <asp:TextBox ID="txtSearchStart" runat="server" CssClass="form-control" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'})"></asp:TextBox>
              </div>
                  
              <div class="col-md-6 form-group">
                <label><asp:Label ID="Label6" runat="server" Text="更新止"></asp:Label></label>
                <asp:TextBox ID="txtSearchEnd" runat="server" CssClass="form-control" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'})"></asp:TextBox>
              </div>
                  
              <div class="pull-right">
                  <asp:LinkButton ID="lnbSearchMore" runat="server" CssClass="btn btn-primary" onclick="lnbSearchMore_Click">搜索</asp:LinkButton>
                  <asp:LinkButton ID="lnbSearchCancel" runat="server" CssClass="btn btn-default" onclick="lnbSearchCancel_Click">重置</asp:LinkButton>
              </div>

          </div>

      </div>

</asp:Content>